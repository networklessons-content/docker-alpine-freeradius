FROM alpine:latest

# Install required packages
RUN apk update && \
    apk add \
    openssl \
    freeradius \
    freeradius-eap \
    freeradius-radclient && \
    rm /var/cache/apk/*

# Change certificates expiration from 60 to 3652 days
RUN \
  sed -i '/default_days/c\default_days            = 3652' /etc/raddb/certs/ca.cnf && \
  sed -i '/default_days/c\default_days            = 3652' /etc/raddb/certs/server.cnf && \
  sed -i '/default_days/c\default_days            = 3652' /etc/raddb/certs/client.cnf

# Generate demo certificates
RUN cd /etc/raddb/certs && \
    ./bootstrap && \
    chown radius:radius *

# Redirect logs to STDOUT
RUN ln -sf /dev/stdout /var/log/radius/radius.log

EXPOSE \
    1812/udp \
    1813/udp

CMD ["radiusd","-xx","-f"]